package com.avenuecode.dao;

import org.springframework.stereotype.Repository;

import com.avenuecode.model.Image;

/**
 * Created by paulo on 18/11/17.
 */

@Repository
public class ImageDaoImpl extends AbstractJpaDAO<Image> implements ImageDao{
	
	
	public ImageDaoImpl() {
        super();

        setClazz(Image.class);
    }

	
	
	
}
