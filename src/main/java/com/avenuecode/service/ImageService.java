package com.avenuecode.service;

import com.avenuecode.model.Image;

import java.util.List;

/**
 * Created by paulo on 18/11/17.
 */
public interface ImageService {

    List<Image> getAll();
    boolean save(Image image);
    Image getById(Integer id);
    void update(Image image);
	void deleteById(Integer entityId);
}
