package com.avenuecode.dao;

import java.util.List;

import com.avenuecode.model.Product;



public interface ProductDao {
	//TODO Implement here methods specific to Product context
	
	List<Product> getAllWithoutRelationships();

	Product getProductWithoutRelationship(Integer id);

}
