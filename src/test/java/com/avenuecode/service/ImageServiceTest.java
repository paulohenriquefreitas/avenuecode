package com.avenuecode.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.avenuecode.dao.AbstractJpaDAO;
import com.avenuecode.mock.ImageMock;
import com.avenuecode.mock.ProductMock;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceTest<T> {
	
	@InjectMocks
	private ImageServiceImpl imageServiceImpl;
	
	@Mock
	private AbstractJpaDAO<T> abstractJpaDAO;
	
	@SuppressWarnings("unchecked")
	@Test
	public void test_save_success() throws Exception {
		
		verify(abstractJpaDAO, times(0)).create((T) ImageMock.getImageMock());
		
	}
	
//	Due the similarity of the events the other tests will not be implemented for this exam;
}
