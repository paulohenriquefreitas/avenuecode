package com.avenuecode.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.avenuecode.dao.ImageDao;
import com.avenuecode.dao.ImageDaoImpl;
import com.avenuecode.dao.ProductDao;
import com.avenuecode.dao.ProductDaoImpl;
import com.avenuecode.service.ImageService;
import com.avenuecode.service.ImageServiceImpl;
import com.avenuecode.service.ProductService;
import com.avenuecode.service.ProductServiceImpl;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by paulo on 18/11/17.
 */

@Configuration
@EnableTransactionManagement
public class AvenueCodeConfig {

    @Bean
    public ImageService imageService() {
        return new ImageServiceImpl();
    };
    
    @Bean
    public ImageDao imageDaoImp() {
    	return new ImageDaoImpl();
    }
    
    @Bean
    public ProductService productService() {
        return new ProductServiceImpl();
    };
    
    @Bean
    public ProductDao productDaoImp() {
    	return new ProductDaoImpl();
    }
    
    @Bean
    public ObjectMapper buildObjectMapper() {
       return new ObjectMapper().setSerializationInclusion(Include.NON_NULL)
    		                    .setSerializationInclusion(Include.NON_EMPTY);
    }
    
    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory() {
        return new HibernateJpaSessionFactoryBean();
    }

  
}
