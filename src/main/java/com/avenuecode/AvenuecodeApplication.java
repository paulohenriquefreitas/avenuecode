package com.avenuecode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;

@SpringBootApplication
public class AvenuecodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvenuecodeApplication.class, args);
	}
}
