package com.avenuecode.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;
import javax.ws.rs.ApplicationPath;

import com.avenuecode.controller.ImageController;
import com .avenuecode.controller.ProductController;

/**
 * Created by paulo on 18/11/17.
 */

@Component
@ApplicationPath("/avenuecode")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(ProductController.class);
        register(ImageController.class);
    }
}
